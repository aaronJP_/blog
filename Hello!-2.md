﻿#Aaron's blog 
---

Welkom op mijn _blog_ mijn naam is **Aaron Plaizier**. Ik ben **20** jaar en dit is mijn eerste jaar op het HBO. Hier ga ik alles in zetten omtrent mijn proces tijdens de studie **CMD**. 

**Week 1:**
De kick-off van deze periode vindt plaats in Maastricht. Ik ben helaas ziek en ik moet de eerste dag aan me voorbij laten schieten. Omdat ik later aansluit heb ik al een aantal opdrachten gemist maar ik ben bijgepraat door mijn teamgenoten in Maastricht. Het uiteindelijke resultaat van onze groep was Nee Nu welness & speelparadijs waar de ouders kunnen genieten van een dag in de welness terwijl de kinderen kunnen spelen in het speelparadijs.  

**Week 2:**
We worden ingedeeld in groepjes en ik zit in de groep met Joshua, Quinty, Hannad en Sico en we noemen onszelf Team G.O.A.T. Omdat ik nog niet helemaal beter ben van vorige week mis ik een studiodag. We hebben ons plan van aanpak laten valideren en hieruit is gekomen dat we meer taken individueel moeten verdelen om zo efficiënt mogelijk te werken. 

**Week 3** 



**VAKANTIE**

**Week 4**
Vandaag ben ik begonnen aan mijn eigen lifestyle diary. De 48 uur die je visueel in kaart moet brengen als begin van het onderzoek. Ik heb mijn lifestyle diary ingedeeld in de categorie thuis, onderweg en school en hierin een tijdlijn gemaakt. De plaatjes die ik hiervoor gebruik verdeel ik weer onder in de categorie eten, bewegen en ontspanning. Deze week heb ik deze ook laten valideren door Joke. De feedback hierop is dat dat mijn mediaprikkels ontbreken en het meer persoonlijk moet zijn. Quinty en ik zijn ook begonnen aan ons onderzoek door interviews te houden. Deze week heb ik ook de workshop samenwerken gevolgd waar we geleerd hebben de samenwerking te analyseren. 

**Week 5**
Vandaag willen we de ontwerpcriteria opstellen. We hebben feedback gevraagd aan Robin die ons heeft verteld dat ons interview te algemeen is. Hij heeft ons laten zien dat we beter moeten inzoomen op 1 thema. Dit gaan we dan ook doen op het thema Goedkoop en gezond boodschappen doen. We gaan nieuwe interview vragen opstellen en meer een diepgaand gesprek voeren i.p.v alleen de vragen afhandelen. Deze week hebben Joshua en ik ook de lifestyle diary voor de toekomstige situatie gemaakt. Deze week heb de workshop data visualisatie gedaan bij Mieke waar we hebben geleerd in welke vormen we data kunnen verwerken om ze naar buiten toe te presenteren. 

**Week 6**
Omdat ons onderzoek was vertraagd is de ontwerpcriteria een week doorgeschoven. Naast de ontwerpcriteria hebben we ook insight cards gemaakt. 


**Week 7**
Aan de hand van de ontwerpcriteria kunnen we een concept gaan bedenken: 

Het concept moet de gebruiker stimuleren om gezond te koken, omdat uit ons onderzoek is
gebleken dat 1 e jaar CMD studenten regelmatig ongezond eten.
Het concept moet de gebruiker tot een gezondere leefstijl toejuichen.
1 e jaar CMD studenten moeten bewust worden van hun huidige leefstijl en moeten bewust
worden welke voedingswaarden ze binnenkrijgen met hun huidige leefstijl.
Het concept moet de gebruiker informeren welke voedingswaarde in bepaalde ingrediënten
zit, omdat uit ons onderzoek is gebleken dat 1 e jaar CMD studenten zich hier niet in
verdiepen.
Het concept moet gesteund worden door kennis van studenten die ervaring hebben op
kamers, omdat uit ons onderzoek is gebleken dat 1 e jaar CMD studenten zich hiermee
kunnen identificeren.

Visueel

Het concept moet door middel van kleuren een nadruk leggen op de belangrijke informatie,
zo is dit makkelijker te vinden. De layout moet de gebruiker (studenten aan CMD) aansporen
en stimuleren tot actie. (Call to action)
Het concept moet foto’s bevatten van studenten die advies hebben gegeven of maaltijden,
dit geeft een persoonlijkere sfeer.

Ons concept is een instagram page die jongeren stimuleert om gezonder en goedkoper te eten. Vandaag heb ik de workshop rapid-prototyping bij Bob gevolgd. Hierin heb ik geleerd hoe ik met zo weinig mogelijk inspanning zoveel mogelijk informatie uit de prototype kunnen halen. Na de workshop heb ik het prototype gemaakt. Dit prototype bestaat uit een instagram page genaamt 'Studentsolutions 2018' waar ik een post heb gedaan van een pasta met de bijpassende ingedienten die deze week in de aanbieding zijn.  

