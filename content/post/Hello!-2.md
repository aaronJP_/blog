#Aaron's blog 
---

Welkom op mijn _blog_ mijn naam is **Aaron Plaizier**. Ik ben **20** jaar en dit is mijn eerste jaar op het HBO. Hier ga ik alles in zetten omtrent mijn proces tijdens de studie **CMD**. 

**4 september 2017, Rotterdam**
>Mijn eerste echte lesdag op de _Hogeschool van Rotterdam_. De dag begint met een introductie over het gebruik en de regels binnen de studio waar we in werken. Ik vind het leuk dat we onze eigen studio hebben omdat we zelf mogen bepalen hoe de studio eruit ziet zo heb ik toch het gevoel alsof het echt onze eigen werkplek is.  Dit is ook gelijk de eerste dag in mijn team **Team Infinity**.

>- Brainstormen 
>- Poster maken 
>- Begonnen aan onderzoek
>- Planning maken
>- Fabian en Ben hebben het onderstaande logo voor ons team gemaakt

**5 September 2017, Rotterdam**
>Op deze dag hebben we alleen een hoorcollege voor design theory. Tijdens dit hoorcollege heb ik aantekeningen gemaakt: ![enter image description here](https://attachment.outlook.office.net/owa/aaron_plaizier@live.nl/service.svc/s/GetAttachmentThumbnail?id=AQMkADAwATM3ZmYAZS05ODMxLTIyAGVhLTAwAi0wMAoARgAAA3E7%2bIyQK8xNpZxrYIGLCv4HADV4mCf9mdJDqGmBJrVNN5UAAAIBCQAAADV4mCf9mdJDqGmBJrVNN5UAAb8Yl/cAAAABEgAQAOUcAvqE6IFHg4UZdRlpGGE=&thumbnailType=2&X-OWA-CANARY=mq4zkn7vhU2oGzFIce4DofAYQ6rx-NQYOJLk64re14D3jzLk6WQ3CSMjeJDbnruRZSik7Wz7QL8.&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImVuaDlCSnJWUFU1aWpWMXFqWmpWLWZMMmJjbyJ9.eyJ2ZXIiOiJFeGNoYW5nZS5DYWxsYmFjay5WMSIsImFwcGN0eHNlbmRlciI6Ik93YURvd25sb2FkQDg0ZGY5ZTdmLWU5ZjYtNDBhZi1iNDM1LWFhYWFhYWFhYWFhYSIsImFwcGN0eCI6IntcIm1zZXhjaHByb3RcIjpcIm93YVwiLFwicHJpbWFyeXNpZFwiOlwiUy0xLTI4MjctMjI5Mzc0LTI1NTMzNTcwMzRcIixcInB1aWRcIjpcIjk4NTE1NjM4MTkwOTczOFwiLFwib2lkXCI6XCIwMDAzN2ZmZS05ODMxLTIyZWEtMDAwMC0wMDAwMDAwMDAwMDBcIixcInNjb3BlXCI6XCJPd2FEb3dubG9hZFwifSIsImlzcyI6IjAwMDAwMDAyLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMEA4NGRmOWU3Zi1lOWY2LTQwYWYtYjQzNS1hYWFhYWFhYWFhYWEiLCJhdWQiOiIwMDAwMDAwMi0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvYXR0YWNobWVudC5vdXRsb29rLm9mZmljZS5uZXRAODRkZjllN2YtZTlmNi00MGFmLWI0MzUtYWFhYWFhYWFhYWFhIiwiZXhwIjoxNTA1MTIwMjAzLCJuYmYiOjE1MDUxMTk2MDN9.7upjTuEY1Px3bt5TsJNpeh0gQXD2KROotDCply3rGkrLw4AWRHFuKO-D5yqinM66W0FRU8tkWnMgE_ZOVDoIEkTElR0YAtwJq4Cum7o4OxoUGPYBJvH7fXD829JuiPYVRsh3UQ38_9c_sOfflGexkvwJtCatT6j8sFgzxhH0a-RidG84paemoJ3_lCxRvMbJeZYTUk2WGJwu86r8qhTChyRJaL8eY0osZy9aEoxy2fAmfl_vV1QsLDtC1ucxbSvW7lWjRPs-PpsAvDfK0yhpMYUl_UB-C2aCU3huMj_PnDW1KDRlDKg1Bskm8FWI_93cRz2SKF3Ofncpcugvlncwug&owa=outlook.live.com&isc=1)
>![enter image description here](https://attachment.outlook.office.net/owa/aaron_plaizier@live.nl/service.svc/s/GetAttachmentThumbnail?id=AQMkADAwATM3ZmYAZS05ODMxLTIyAGVhLTAwAi0wMAoARgAAA3E7%2bIyQK8xNpZxrYIGLCv4HADV4mCf9mdJDqGmBJrVNN5UAAAIBCQAAADV4mCf9mdJDqGmBJrVNN5UAAb8Yl/cAAAABEgAQABD84fZIV%2b1JnIOpniVp7Wk=&thumbnailType=2&X-OWA-CANARY=mq4zkn7vhU2oGzFIce4DofAYQ6rx-NQYOJLk64re14D3jzLk6WQ3CSMjeJDbnruRZSik7Wz7QL8.&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImVuaDlCSnJWUFU1aWpWMXFqWmpWLWZMMmJjbyJ9.eyJ2ZXIiOiJFeGNoYW5nZS5DYWxsYmFjay5WMSIsImFwcGN0eHNlbmRlciI6Ik93YURvd25sb2FkQDg0ZGY5ZTdmLWU5ZjYtNDBhZi1iNDM1LWFhYWFhYWFhYWFhYSIsImFwcGN0eCI6IntcIm1zZXhjaHByb3RcIjpcIm93YVwiLFwicHJpbWFyeXNpZFwiOlwiUy0xLTI4MjctMjI5Mzc0LTI1NTMzNTcwMzRcIixcInB1aWRcIjpcIjk4NTE1NjM4MTkwOTczOFwiLFwib2lkXCI6XCIwMDAzN2ZmZS05ODMxLTIyZWEtMDAwMC0wMDAwMDAwMDAwMDBcIixcInNjb3BlXCI6XCJPd2FEb3dubG9hZFwifSIsImlzcyI6IjAwMDAwMDAyLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMEA4NGRmOWU3Zi1lOWY2LTQwYWYtYjQzNS1hYWFhYWFhYWFhYWEiLCJhdWQiOiIwMDAwMDAwMi0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvYXR0YWNobWVudC5vdXRsb29rLm9mZmljZS5uZXRAODRkZjllN2YtZTlmNi00MGFmLWI0MzUtYWFhYWFhYWFhYWFhIiwiZXhwIjoxNTA1MTIwMjAzLCJuYmYiOjE1MDUxMTk2MDN9.7upjTuEY1Px3bt5TsJNpeh0gQXD2KROotDCply3rGkrLw4AWRHFuKO-D5yqinM66W0FRU8tkWnMgE_ZOVDoIEkTElR0YAtwJq4Cum7o4OxoUGPYBJvH7fXD829JuiPYVRsh3UQ38_9c_sOfflGexkvwJtCatT6j8sFgzxhH0a-RidG84paemoJ3_lCxRvMbJeZYTUk2WGJwu86r8qhTChyRJaL8eY0osZy9aEoxy2fAmfl_vV1QsLDtC1ucxbSvW7lWjRPs-PpsAvDfK0yhpMYUl_UB-C2aCU3huMj_PnDW1KDRlDKg1Bskm8FWI_93cRz2SKF3Ofncpcugvlncwug&owa=outlook.live.com&isc=1)

 **6 September 2017, Rotterdam**
> Onze 2e echte studiodag. Zoals elke studiodag beginnen we met een gezamelijke start en een planning van de dag. Het is voor mij nog even wennen omdat ik nog nooit op zo'n manier les heb gevolgd maar ik ervaar dit als zeer prettig.  Je hebt **vrijheid** en je eigen **verantwoordelijkheid**. Vandaag heb ik mijn moodboard zo goed als afgemaakt. Ik heb vandaag ook onderzoek gedaan naar de *nachtclubs en leuke cafe's * voor ons spel. Ik heb dit onderzoek ook zo goed als afgerond en visueel gemaakt. Ik ben ook begonnen met deze blog om alles bij te houden in mijn proces.

> - Moodboard gemaakt
> - Visueel onderzoek gemaakt
> - Blog begonnen

---
##Week 2 

**strong text**
